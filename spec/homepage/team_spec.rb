describe Gitlab::Homepage::Team do
  subject { described_class.build! }

  describe '#members' do
    it 'returns an array of team members' do
      expect(subject.members).to all(be_a Gitlab::Homepage::Team::Member)
    end
  end

  describe '#projects' do
    it 'returns a list of projects with members assigned' do
      expect(subject.projects.first.key).to eq 'gitlab-ce'
      expect(subject.projects.first.maintainers.count).to be_nonzero
    end
  end

  describe '.build!' do
    it 'builds a team object by reading team.yml' do
      expect(subject).to be_a described_class
      expect(subject.members).not_to be_empty
    end
  end
end
