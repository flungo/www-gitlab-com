require 'yaml'

module Gitlab
  module Homepage
    class Team
      def initialize(data)
        @data = data
      end

      def members
        @members ||= @data.map do |entry|
          Team::Member.new(entry)
        end
      end

      def projects
        @projects ||= Team::Project.all! do |project|
          members.each do |member|
            project.assign(member) if member.involved?(project)
          end
        end
      end

      def self.build!
        self.new(YAML.load_file('data/team.yml'))
      end
    end
  end
end
