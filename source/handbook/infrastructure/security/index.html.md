---
layout: markdown_page
title: "Security Team"
---

## Common Links

- Security crosses many teams in the company, but most often security related issues are posted in the [public Infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure/issues/), or [gitlab-ce], or even [organization] trackers with the `~security` label. Please use confidential issues for topics that should only be visible to team members at GitLab.
- [Chat channel](https://gitlab.slack.com/archives/security); please use the `#security` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.
- [Frequently asked questions about Security of GitLab](/security) _the application_  as well as GitLab _the organization_  are documented on the top-level [security page](/security).
- [Security Best Practices](/handbook/security), using 1Password and similar tools, are documented on their own [security best practices page](/handbook/security).

## On this page
{:.no_toc}

- TOC
{:toc}

## Security topics

At a high level, the topic of security encompasses the

1. application
2. infrastructure of GitLab.com
3. organization and internal processes

## Issue Triage

The Security team needs to be able to communicate the priorities of security related issues to the Product, Development, and Infrastructure teams. Here's how the team can set priorities internally for subsequent communication (inspired in part by how the [support team does this](/handbook/support/workflows/support_workflows/issue_escalations.html.md)).

### Use labels

Always. Use `~security` and as appropriate also `~bug`, `~feature proposal`, `~customer`. Add [Security Level priority labels](#security-priority-labels) ( `~SL1`, `~SL2`, `~SL3`) to indicate perceived priority inside the Security Team.

The reasoning behind adding an `~SL` label to _every_ of these issues is that each issue _should_ have had
someone consider the urgency and impact, and this is best done at time of creating the issue since that is
when the information and context is "fresh" in your mind. It is OK to change the assessment and label at a
later date upon reflection. When issues are filed _without_  an `~SL` label it will be unclear whether an issue lacks the label due to lack of urgency / impact, or due to missing a step in the process.


### Security Priority Labels

Use the following as a guideline to determine which Security Level Priority label to use for bugs and feature proposals. For this, consider the _likelihood_ and _impact_  of a security incident that could result from this issue not being resolved.

- **Likelihood:**  _For example: If left unresolved, can we expect to see an incident within a release cycle?_
  - L1 - Wide open gap easily found, or perhaps even ways for the incident to be triggered _accidentally_ .
  - L2
  - L3 - Unlikely an incident would occur (hard to find vulnerability, super edge cases).
- **Impact:** _For example: Can data be lost or compromised as a result?_
  - I1 - Any data loss. Or data exposed of many users.
  - I2
  - I3 - Meta data exposed (e.g. number of merge requests pushed) of some users

| **Likelihood \ Impact**          | **I1 - High** | **I2 - Medium**  | **I3 - Low**   |
|-------------------------------|---------------|------------------|----------------|
| **L1 - High**                 | `SL1`         | `SL1`            | `SL2`          |
| **L2 - Medium**               | `SL1`         | `SL2`            | `SL3`          |
| **L3 - Low**                  | `SL2`         | `SL3`            | `SL3`          |

#### Escalating from the Security Team to the Development Team

**Note**
- Issues are not scheduled for a particular release unless the team leads add them to a release milestone
*and* they are assigned to a developer.
- Safety valve: If something is "truly urgent", pinging leads in the issues when they are created is the best
way to go, so it can be made Next Patch Release. This will often be preceded by loud debate and concurrence on
chat.

Issues with an `SL1` or `SL2` rating should be immediately brought to the attention
of the relevant product managers, and team leads by pinging them in the issue
and/or escalating via email and chat if they are unresponsive.

Issues with an `SL1` rating are given priority over all other releases and should
be patched immediately.

Issues with an `SL2` rating will be scheduled for the next security release, which may
be days or weeks ahead depending on severity and other issues that are waiting for
patches. An `SL2` rating is not a guarantee that a patch will be ready prior to
the next security release, but that should be the goal.

Issues with an `SL3` rating have a lower sense of urgency and are assigned a target
of the next minor version. If a low-risk or low-impact vulnerability is reported that would
normally be rated `SL3` but the researcher has provided a 30 day time window (or less)
for disclosure this issue may receive an `SL2` rating to ensure that it is
patched before disclosure. Any `SL3` issues that are approaching their public disclosure
window can be re-assigned an `SL2` rating.

An `SL4` rating is also available. `SL4` is used for issues that are suggestions
to improve security without a well-defined path for implementation, vulnerabilities
that are low risk but would require complex changes to the application or
architecture to fix, or other long-term issues. `SL4` issues have no defined
schedule for closure.

#### More Risk Rating Examples

`SL1`:
* Remote Command Execution (RCE)
* SQL Injection (SQLi)
* Authentication Bypass
* Authorization vulnerabilities that expose critical data (password hashes, repositories, tokens)

`SL2`:
* Cross-site Scripting (XSS)
* Authorization vulnerabilities that do not expose critical data
* Resource exhaustion denial-of-service (DoS)
* Cross-site Request Forgery (CSRF)

`SL3`:
* Tab nabbing
* Race conditions that do not put user data in jeopardy
* Path disclosure

`SL4`:
* Implement new security feature
* Remove support for dangerous protocol

## Security releases

The processes for security releases is described with a checklist of events on the [critical release process](/handbook/engineering/critical-release-process) page, as well as in the [release-tools](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/security.md) project.

## Fighting Spam

The security team plays a large role in defining procedures for defending against
and dealing with spam. Common targets for spam are public snippets, projects, issues,
merge requests, and comments. Advanced techniques for dealing with these types of spam
are detailed in the [Spam Fighting runbook](https://docs.google.com/document/d/1V0X2aYiNTZE1npzeqDvq-dhNFZsEPsL__FqKOMXOjE8).

## Risk Assessments

GitLab conducts regular Risk Assessments and all team members are encouraged to
participate. The Risk Assessment consists of a list of all risks or threats to
the GitLab infrastructure and GitLab as a company, their likelihood of occuring,
the impact should they occur, and what actions can be taken to prevent these
risks from damaging the company or mitigate the damage should they be realized.

All Risk Assessments are stored as Google Sheets and are available to all team
members. Risk Assessments should not be shared with people outside of the company
without permission.

[Current Risk Assessment](https://docs.google.com/a/gitlab.com/spreadsheets/d/1xtuoemhFhyh7Og-76pQhHWtQHkaaeU9uqjyuJkvNfS0/edit?usp=sharing)

The format of the Risk Assessment may seem intimidating at first. If you do not
know what values to use for risk ratings, impact ratings, likelihoods or any other
value leave them blank and they will be filled in by the appropriate team. It
is more important to have all risks documented than it is to have all values
completed when adding new risks. Guidelines and instructions for how to add a 
risk and how to calculate each rating or score are included on the 
"Instructions" sheet.
