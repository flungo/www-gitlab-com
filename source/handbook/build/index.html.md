---
layout: markdown_page
title: "Build"
---

## Common links

* [Build issue tracker](https://gitlab.com/gitlab-org/omnibus-gitlab/issues). If
you need to submit sensitive issue, please use confidential issues.
* [Slack chat channel](https://gitlab.slack.com/archives/build)

## Team responsibility

Build is about how we ship GitLab, and making sure everyone can easily install,
update and maintain GitLab.

This means:

- Make Omnibus packages
- Maintain the official installations on our [installation page](https://about.gitlab.com/installation/)
- Make sure nightly builds are installed on dev.gitlab.org
- Keep the installation and download pages up to date and attractive

Our official installations cover:

1. Omnibus packages for LTS versions of all major Linux operating systems
(Ubuntu, Debian,CentOS/RHEL, OpenSUSE, SLES) and
instructions for Ubuntu on Windows 10
1. Docker Images and images for all major clouds (AWS, GCP, Azure)
1. A helm chart for Kubernetes and instructions for major container schedulers
(GKE, OpenShift, Tectonic, kubeadm, Docker Swarm, Mesosphere DC/OS)
1. Pivotal Cloud Foundry tile.

## How to work with Build

Everything that is done in GitLab will end up in the packages that are shipped
to users. While that sounds like the last link in the chain, it is one of the
most important ones. This means that informing the Build team of a change in an
early stage is crucial for releasing your feature. While last minute changes are
inevitable and can happen, we should strive to avoid them.

We expect every team to reach out to the Build team before scheduling a feature
in an upcoming release in the following cases:

* The change requires a new or an update on a gem with native extensions.
* The change requires a new or updated external software dependency.
  * Also when the external dependency has its own external dependencies.
* The change adds, modifies, or removes files that should be managed by
omnibus-gitlab. For example:
  * The change introduces new directories in the package.
* The change requires a new configuration file.
* The change requires a change in a previously established configuration.

To sum up the above list:

If you need to do `install`, `update`, `make`, `mkdir`, `mv`, `cp`, `chown`,
`chmod`, compilation or configuration change in any part of GitLab stack, you
need to reach out to the Build team for opinion as early as possible.

This will allow us to schedule appropriately the changes (if any) we have to make
to the packages.

If a change is reported late in the release cycle or not reported at all,
your feature/change might not be shipped within the release.

If you have any doubt whether your change will have an impact on the Build team,
don't hesitate to ping us in your issue and we'll gladly help.

TODO's

1. Installation page visuals https://gitlab.com/gitlab-com/www-gitlab-com/issues/1074
1. Lower the barrier of contributing to the Build team task
  * First goal, allow simpler creation of packages: https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2234

## Internal team training

Every Build team member is responsible for creating a training session for the
rest of the team. These trainings will be recorded and available to the whole
team.

### Purpose

The purpose of team training is to introduce the work done to the rest of your team.
It also allows the rest of the team to easily transition into new features and projects
and prepare them for maintenance.

Training should be:

* Providing a high level overview of the invested work
* Describing the main challenges encountered during development
* Explaining the possible pit-falls and specificities

Training *should not* be:

* Replacement for documentation
* Replacement for writing down progress in issues
* Replacement for follow up issues

Simply put, the training is a summation of: notes taken in issues during development,
programming challenges, high level overview of written documentation. Your team
member should be able to take over the maintenance or build on top of your feature
with less effort after they have been part of the training.

*Note* Do not shy away from being technical in your training. You can ask yourself:
What would have been useful for me when I started working on this task? What
would have helped me be more efficient?

### Efficiency of the training

In order to see whether the training is efficient, Build lead will rotate team
members on projects where training was done. For example, if the feature
requires regular releases, the person who gave the training will be considered
a tutor. Different team member will follow the training and documentation and
will ask the original maintainer for help. The new person responsible is now
also responsible for improving the feature. They are now also responsible of
training other team members.

### FAQ

Q: Isn't this double work?
A: No. The training should be prepared while documenting the task.

Q: Won't this slow me down?
A: At the beginning, possibly. However, every hour of the training given will
multiple the value of it by the amount of team members.

Q: Isn't it more useful to let the team check out the docs and ask questions?
A: In an ideal world, possibly. However, everyone has a lot of tasks assigned
and they might not be able to go through the docs until they need to do something.
This might be months later and you, as a person who would give the training, might not
be able help efficiently anymore.


## Cloud Images

The process documenting the steps necessary to update the GitLab images available on
the various cloud providers is detailed on our [Cloud Image Process page](https://about.gitlab.com/cloud-images/).
