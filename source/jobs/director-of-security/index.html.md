---
layout: job_page
title: "Director of Security"
---

The Director of Security [reports to the VP of Engineering](/team/structure/).

Our thesis is that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e).
We think that simulating a security culture in engineering is one of the most
important things. We don't do checklist security, the goal is to keep the trust
of our users by being secure, compliance is not a goal in itself. We don't think
that third party products are unimportant but they are not a silver bullet to
making everything secure.

As Director of Security at GitLab, you will succeed if you enable, enact, and
evangelize such holistic security around the application, GitLab.com, and the
organization as a whole such that GitLab and GitLab.com are established as being
 at the vanguard of security thinking and practices.

## Responsibilities

- Collaboratively define company-wide scope and priorities for security related
activities.
- Coordinate efforts to monitor and increase the security of
GitLab's products (GitLab.com SaaS, on-premise application), and organization.
- Determine appropriate combination of internal security efforts and external
security efforts including bug bounty programs, external security audits
(penetration testing, black box, white box testing).
- Facilitate cross-team collaboration and escalation.
- Analyze and advise on new security technologies and program conformance.
- Build and manage a team, which currently consists of our [Security Lead](https://about.gitlab.com/jobs/security-lead)
and [Security Specialists](https://about.gitlab.com/jobs/security-specialist)(vacancy).
   - Identify and fill positions.
   - Grow skills in team leads and individual contributors, for example by
   creating training and testing materials.
   - Deliver input on promotions, function changes, demotions, and terminations.
- Enable all engineers and the public at large to understand security and abuse incidents.
- Ensure security issues and best practices are well-documented.

## Requirements

- Significant application and SaaS security experience in production-level settings.
- This position does not require extensive development experience but the
applicant should be very familiar with common security libraries, security
controls, and common security flaws that apply to Ruby on Rails applications.
- Experience managing teams of engineers.
- Excellent communication skills, both written and oral, with a specific ability
to communicate clearly about technical issues.
- Proven track record of delivering high-value, high-impact, cross-team projects.
- Passion for open source.
- Familiar with distributed teams.
- You share our [values](/handbook/values), and work in accordance with those values.

### Hiring Process

The hiring process for this role consists of _at least_

- [screening call](/handbook/hiring/#screening-call)
- interview with Security Lead
- interview with Director of Infrastructure
- interview with VP of Engineering
- interview with CEO

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
