(function () {
  var hash = window.location.hash;
  if (hash) {
    $('.omnibus-table a[href="' + hash.replace('omnibus-', '') + '"]').tab('show');
  }

  $('.omnibus-table').on('shown.bs.tab', function(e) {
    window.location.hash = e.target.hash.replace('#', '#omnibus-');
  });
})();
