gitlab-ce:
  name: GitLab Community Edition (CE)
  path: gitlab-org/gitlab-ce
  link: https://gitlab.com/gitlab-org/gitlab-ce
  mirrors:
    - https://github.com/gitlabhq/gitlabhq
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is the community edition of GitLab.

    Most of the development happens here, then gets merged into GitLab EE
    periodically. Unless you're making a change specific to GitLab EE, add it to CE.

gitlab-ee:
  name: GitLab Enterprise Edition (EE)
  path: gitlab-org/gitlab-ee
  link: https://gitlab.com/gitlab-org/gitlab-ee
  description: |
    This is _not_ an open source project, but we made the source code available for
    viewing and contributions.

    GitLab Community Edition is merged daily to GitLab Enterprise Edition.

    GitLab EE requires a license key to be used.

gitlab-shell:
  name: GitLab Shell
  path: gitlab-org/gitlab-shell
  link: https://gitlab.com/gitlab-org/gitlab-shell
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlab-shell
    - https://github.com/gitlabhq/gitlab-shell
  description: |
    GitLab Shell handles Git commands for GitLab. It's an essential part of GitLab.

gitlab-workhorse:
  name: GitLab Workhorse
  path: gitlab-org/gitlab-workhorse
  link: https://gitlab.com/gitlab-org/gitlab-workhorse
  description: |
    Gitlab-workhorse is a smart reverse proxy for GitLab. It handles "large" HTTP
    requests such as file downloads, file uploads, Git push/pull and Git archive
    downloads.

omnibus-gitlab:
  name: Omnibus GitLab
  path: gitlab-org/omnibus-gitlab
  link: https://gitlab.com/gitlab-org/omnibus-gitlab
  mirrors:
  - https://dev.gitlab.org/gitlab/omnibus-gitlab
  - https://github.com/gitlabhq/omnibus-gitlab
  description: |
    Omnibus GitLab creates the packages for GitLab.

cookbook-omnibus-gitlab:
  name: Cookbook Omnibus GitLab
  path: gitlab-org/cookbook-omnibus-gitlab
  link: https://gitlab.com/gitlab-org/cookbook-omnibus-gitlab
  description: Omnibus Chef Cookbooks

gitlab-development-kit:
  name: GitLab Development Kit
  path: gitlab-org/gitlab-development-kit
  link: https://gitlab.com/gitlab-org/gitlab-development-kit
  description: |
    GitLab Development Kit (GDK) provides a collection of scripts and other
    resources to install and manage a GitLab installation for development
    purposes. The source code of GitLab is spread over multiple repositories
    and it requires Ruby, Go, Postgres/MySQL, Redis and more to run.

    GDK helps you install and configure all these different components,
    and start/stop them when you work on GitLab.

gitaly:
  name: Gitaly
  path: gitlab-org/gitaly
  link: https://gitlab.com/gitlab-org/gitaly
  description: |
    Git RPC service for handling all the git calls made by GitLab.

gitlab-qa:
  name: GitLab QA
  path: gitlab-org/gitlab-qa
  link: https://gitlab.com/gitlab-org/gitlab-qa
  description: |
    End-to-end, black-box, entirely click-driven integration tests for GitLab.

www-gitlab-com:
  name: GitLab Inc. Homepage
  path: gitlab-com/www-gitlab-com
  link: https://gitlab.com/gitlab-com/www-gitlab-com
  description: GitLab Inc. Homepage available at about.gitlab.com

marketo-tools:
  name: Marketo Tools
  path: gitlab-com/marketo-tools
  link: https://gitlab.com/gitlab-com/marketo-tools
  description: Internal Marketo Tools

githost:
  name: GitHost.io
  path: gitlab-com/githost
  link: https://gitlab.com/gitlab-com/githost
  description: Hosted version of GitLab

gitlab-pages:
  name: GitLab Pages
  path: gitlab-org/gitlab-pages
  link: https://gitlab.com/gitlab-org/gitlab-pages
  description: |
    GitLab Pages daemon used to serve static websites for GitLab users

gitlab-runner:
  name: GitLab Runner
  path: gitlab-org/gitlab-ci-multi-runner
  link: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner
  description: GitLab CI/CD Runner

license-app:
  name: License App
  path: gitlab/license-app
  link: https://dev.gitlab.org/gitlab/license-app
  description: Internal GitLab License App

payment-app:
  name: Payment App
  path: gitlab/payment_app
  link: https://dev.gitlab.org/gitlab/payment_app
  description: Internal GitLab Payment App

gitlab-license:
  name: GitLab License
  path: gitlab/gitlab-license
  link: https://dev.gitlab.org/gitlab/gitlab-license
  description: Internal GitLab License Distribution App

university:
  name: GitLab University
  path: gitlab-org/university
  link: https://gitlab.com/gitlab-org/university
  description: Internal GitLab University

version-gitlab-com:
  name: version.gitlab.com
  path: gitlab/version-gitlab-com
  link: https://dev.gitlab.org/gitlab/version-gitlab-com
  description: Internal GitLab Version App

gitlab-contributors:
  name: GitLab Contributors
  path: gitlab-com/gitlab-contributors
  link: https://gitlab.com/gitlab-com/gitlab-contributors
  description: Application behind contributors.gitlab.com

gitlab-templates:
  name: GitLab CI/CD Templates
  path: gitlab-org/gitlab-ci-yml
  link: https://gitlab.com/gitlab-org/gitlab-ci-yml
  description: GitLab CI/CD Templates

pages-gitlab-io:
  name: pages.gitlab.io
  path: pages/pages.gitlab.io
  link: https://gitlab.com/pages/pages.gitlab.io
  description: GitLab Pages landing page

gitlab-anti-spam-toolkit:
  name: GitLab Anti-Spam Toolkit
  path: MrChrisW/gitlab-anti-spam-toolkit
  link: https://gitlab.com/MrChrisW/gitlab-anti-spam-toolkit
  description: GitLab Anti-Spam Toolkit

gitlab-monitor:
  name: GitLab Monitor
  path: gitlab-org/gitlab-monitor
  link: https://gitlab.com/gitlab-org/gitlab-monitor
  description: Tooling used to monitor gitlab.com

gitlab-docs:
  name: GitLab Docs
  path: gitlab-com/gitlab-docs
  link: https://gitlab.com/gitlab-com/gitlab-docs
  description: Project behind docs.gitlab.com

release-tools:
  name: GitLab Release Tools
  path: gitlab-org/release-tools
  link: https://gitlab.com/gitlab-org/release-tools
  description: Instructions and tools for releasing GitLab
